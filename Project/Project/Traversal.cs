﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;
using Project.WPF.Tools;
using System.Windows.Shapes;

namespace Project.WPF
{
    internal abstract class Traversal
    {
        public TextBlock         currentTxtBlock { get; private set; }
        public List<GraphShape>  graphShapes { get; private set; }
        protected bool[]         traversal { get; set; }
        protected Tool           tool { get; set; }
        protected List<Shape>    tree { get; set; }
        protected ShapeRepo ShapeRepo { get; set; }

        public Traversal(Tool tool, TextBlock txtBlock, ShapeRepo shapeRepo)
        {
            this.currentTxtBlock = txtBlock;
            this.graphShapes = shapeRepo.GetVertexShapes();
            traversal = new bool[graphShapes.Count];
            tree = new List<Shape>();
            this.tool = tool;
            this.ShapeRepo = shapeRepo;
        }

        //метод помечает все вершины графа как непросмотреные
        public void NovSet()
        {
            for (int i = 0; i < graphShapes.Count; i++)
            {
                traversal[i] = true;
            }
        }

        public abstract void Execute(int v);
    }
}
