﻿using System;
using System.Linq;
using System.Windows.Controls;
using Project.WPF.Tools;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Timers;
using System.Threading.Tasks;

namespace Project.WPF
{
    internal class DepthTraversal : Traversal
    {
        public DepthTraversal(Tool tool, TextBlock txtBlock, ShapeRepo shapeRepo) : base(tool, txtBlock, shapeRepo) { }
        public override void Execute(int v)
        {
            currentTxtBlock.Text += "[ОБХОД ГРАФА В ГЛУБИНУ]\n";

            currentTxtBlock.Text += "Помечаем все вершины графа как непросмотренные\n";
            NovSet();
            currentTxtBlock.Text += "Запускаем алгоритм обхода графа в глубину\n";
            Dfs(v, 0);
            currentTxtBlock.Text += "\n";
        }

        //реализация алгоритма обхода графа в глубину
        public void Dfs(int v, int count)
        {
            if (traversal.Length == 0)
            {
                currentTxtBlock.Text += "Граф пуст\n";
                return;
            }

            currentTxtBlock.Text += "Просматриваем текущую вершину ";
            currentTxtBlock.Text += $"{v}\n";

            currentTxtBlock.Text += "Помечаем ее как просмотренную\n";
            traversal[v - ShapeRepo.MinIndex - count] = false;

            currentTxtBlock.Text += $"В матрице смежности просматриваем строку с номером {v}\n";
            count = 0;
            for (int u = ShapeRepo.MinIndex; u < ShapeRepo.MaxIndex; u++)
            {
                GraphShape? graphShapeV = graphShapes.FirstOrDefault(shape => shape.Vertex.Id == v);
                GraphShape? graphShapeU = graphShapes.FirstOrDefault(shape => shape.Vertex.Id == u);

                if (graphShapeU is null)
                {
                    count++;
                    currentTxtBlock.Text += $"Вершины с индексом {u} не существует\n";
                    continue;
                }

                currentTxtBlock.Text += $"Проверка смежные ли вершины {v} и {u}, к тому же не просмотрена ли вершина {u}\n";
                if ((graphShapeV.Vertex.RelativesIds.Exists(x => x.Id == graphShapeU.Vertex.Id) || graphShapeU.Vertex.RelativesIds.Exists(x => x.Id == graphShapeV.Vertex.Id))
                    && traversal[u - ShapeRepo.MinIndex - count])
                {
                    currentTxtBlock.Text += $"Вершины {v} и {u} смежные, а вершина {u} не просмотрена.\n\nРекурсивно просматриваем вершину {u}\n";

                    tree.Add(graphShapes.FirstOrDefault(shape => shape.Vertex.Id == u).GridShape.Children.OfType<Ellipse>().FirstOrDefault());
                    tree.Add(ShapeRepo.FindConnectionInfo(graphShapeV, graphShapeU).Connection);
                    tool.DrawHoverEffect(tree);

                    Dfs(u, count);

                    currentTxtBlock.Text += $"Рекурсивный обход вершины {u} закончен\n\n";
                }
            }
        }
    }
}
