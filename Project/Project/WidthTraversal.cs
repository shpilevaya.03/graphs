﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using Project.Core;
using Project.WPF.Tools;

namespace Project.WPF
{
    internal class WidthTraversal : Traversal
    {
        public WidthTraversal(Tool tool, TextBlock txtBlock, ShapeRepo shapeRepo) : base(tool, txtBlock, shapeRepo) { }
        public override void Execute(int v)
        {
            currentTxtBlock.Text += "[ОБХОД ГРАФА В ШИРИНУ]\n";

            currentTxtBlock.Text += "Помечаем все вершины графа как непросмотренные\n";
            NovSet();
            currentTxtBlock.Text += "Запускаем алгоритм обхода графа в ширину\n";
            Bfs(v);
            currentTxtBlock.Text += "\n";
        }

        //реализация алгоритма обхода графа в ширину
        public void Bfs(int v)
        {
            if (traversal.Length == 0) {
                currentTxtBlock.Text += "Граф пуст\n";
                return;
            }

            QueueStruct<int> q = new QueueStruct<int>();
            currentTxtBlock.Text += "Инициализируем очередь\n";
            q.Push(v);
            currentTxtBlock.Text += $"Помещаем вершину {v} в очередь\n";
            traversal[v - ShapeRepo.MinIndex] = false;
            currentTxtBlock.Text += $"Помечаем вершину {v} как просмотренную\nПока очередь не пуста\n";
            while (!q.IsEmpty())
            {
                v = q.Pop();
                currentTxtBlock.Text += "Извлекаем вершину из очереди\n";
                currentTxtBlock.Text += $"Просматриваем эту вершину - {v}\n";
                currentTxtBlock.Text += "Находим все вершины\n";

                var count = 0;
                for (int u = ShapeRepo.MinIndex; u < ShapeRepo.MaxIndex; u++)
                {
                    currentTxtBlock.Text += $"Eсли вершины {v} и {u} смежные и ещё не просмотренные\n";
                    GraphShape? graphShapeV = graphShapes.FirstOrDefault(shape => shape.Vertex.Id == v);
                    GraphShape? graphShapeU = graphShapes.FirstOrDefault(shape => shape.Vertex.Id == u);

                    if (graphShapeU is null)
                    {
                        count++;
                        currentTxtBlock.Text += $"Вершины с индексом {u} не существует\n";
                        continue;
                    }

                    if ((graphShapeV.Vertex.RelativesIds.Exists(x => x.Id == graphShapeU.Vertex.Id) || graphShapeU.Vertex.RelativesIds.Exists(x => x.Id == graphShapeV.Vertex.Id)) && traversal[u - ShapeRepo.MinIndex - count])
                    {
                        //помещаем их в очередь
                        currentTxtBlock.Text += $"Помещаем вершину {u} в очередь";
                        q.Push(u);
                        //и помечаем как просмотренные

                        tree.Add(graphShapes.FirstOrDefault(shape => shape.Vertex.Id == u).GridShape.Children.OfType<Ellipse>().FirstOrDefault());
                        tree.Add(ShapeRepo.FindConnectionInfo(graphShapeV, graphShapeU).Connection);
                        tool.DrawHoverEffect(tree);

                        currentTxtBlock.Text += $"Вершина {u} помечается как просмотренная";
                        traversal[u - ShapeRepo.MinIndex - count] = false;
                    }
                }
            }
        }
    }
}
