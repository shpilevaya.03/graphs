﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Project.Core
{
    public enum OperationType
    {
        [Description("Обход графа в ширину")]
        WidthTraversal,
        [Description("Обход графа в глубину")]
        DepthTraversal,
    }
}
